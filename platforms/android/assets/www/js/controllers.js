var app = angular.module('starter.controllers',['ngCordova']);

localStorage.setItem("user",'');


app.value("misReservas",[]);



app.controller('LoginCtrl', function($scope, LoginService, $ionicPopup, $firebaseArray, $state) {
    $scope.data = {};


    $scope.login = function() {
        LoginService.loginUser($scope.data.username, $scope.data.password).success(function(data) {
          //$state.go('appAdmin.inicioAdmin');
          if(data == "Administrador"){
            localStorage.setItem("user", $scope.data.username);
            $state.go('appAdmin.inicioAdmin');
            console.log("Admin");
          }else{
            localStorage.setItem("user", $scope.data.username);
            $state.go('app.inicio');
            console.log("User");
          }




        }).error(function(data) {
            var alertPopup = $ionicPopup.alert({
                title: 'Logeo Fallido!',
                template: 'Por favor verifique sus credenciales!'
            });
        });
    }



})

app.controller('AppCtrl', function($scope, $ionicModal, $timeout, $ionicLoading, $state, $ionicPopup) {

  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //$scope.$on('$ionicView.enter', function(e) {
  //});
  $scope.logOut = function(){
      ionic.Platform.exitApp()
  };



  // Form data for the login modal
  $scope.loginData = {};

  // Create the login modal that we will use later
  $ionicModal.fromTemplateUrl('templates/login.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modal = modal;
  });

  // Triggered in the login modal to close it
  $scope.closeLogin = function() {
    $scope.modal.hide();
  };

  // Open the login modal
  $scope.login = function() {
    $scope.modal.show();
  };

  // Perform the login action when the user submits the login form
  $scope.doLogin = function() {
    console.log('Doing login', $scope.loginData);

    // Simulate a login delay. Remove this and replace with your login
    // code if using a login system
    $timeout(function() {
      $scope.closeLogin();
    }, 1000);
  };

    //POPUP PARA CAMBIAR CONTRASEÑA DE USUARIOS


    var user = localStorage.getItem("user"); //obtiene la variable de sessión del cookie, donde tiene el usuario
    user = user.replace("\"","") //el cookie devuelve el user con comillas y para firebase no se ocupan entonces se recortan
    user = user.replace("\"","") //el anterior solo quita una de las dos

    var USERS_LOCATION = 'https://sideappprueba.firebaseio.com/Persona/'+user; //se completa el url de firebase con el user obtenido

    var usersRef = new Firebase(USERS_LOCATION);

    //GENERAR EL POPUP DEL CAMBIAR CONTRASEÑA
    // When button is clicked, the popup will be shown...
   $scope.showPrompt = function() {
            $ionicPopup.prompt({
              title: 'Cambiar Contraseña',
            }).then(function(res) {
               res = res.replace("\"","") //el cookie devuelve el user con comillas y para firebase no se ocupan entonces se recortan
               res = res.replace("\"","") //el anterior solo quita una de las dos

              usersRef.update({ Contrasena:res }); //se actualiza el dato sin afectar a los demás
              console.log('Your name is', res);

            });
    };


})




app.controller('AppAdminCtrl', function($scope, $ionicModal, $timeout, $ionicLoading, $state,$ionicPopup) {

  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //$scope.$on('$ionicView.enter', function(e) {
  //});
  $scope.logOut = function(){
      ionic.Platform.exitApp()
  }

  // Form data for the login modal
  $scope.loginData = {};

  // Create the login modal that we will use later
  $ionicModal.fromTemplateUrl('templates/login.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modal = modal;
  });

  // Triggered in the login modal to close it
  $scope.closeLogin = function() {
    $scope.modal.hide();
  };

  // Open the login modal
  $scope.login = function() {
    $scope.modal.show();
  };

  // Perform the login action when the user submits the login form
  $scope.doLogin = function() {
    console.log('Doing login', $scope.loginData);

    // Simulate a login delay. Remove this and replace with your login
    // code if using a login system
    $timeout(function() {
      $scope.closeLogin();
    }, 1000);
  };

  //POPUP PARA CAMBIAR CONTRASEÑA DE USUARIOS


  var user = localStorage.getItem("user"); //obtiene la variable de sessión del cookie, donde tiene el usuario
  user = user.replace("\"","") //el cookie devuelve el user con comillas y para firebase no se ocupan entonces se recortan
  user = user.replace("\"","") //el anterior solo quita una de las dos

  var USERS_LOCATION = 'https://sideappprueba.firebaseio.com/Persona/'+user; //se completa el url de firebase con el user obtenido

  var usersRef = new Firebase(USERS_LOCATION);

  //GENERAR EL POPUP DEL CAMBIAR CONTRASEÑA
  // When button is clicked, the popup will be shown...
  $scope.showPrompt = function() {
    $ionicPopup.prompt({
      title: 'Cambiar Contraseña',
    }).then(function(res) {
      res = res.replace("\"","") //el cookie devuelve el user con comillas y para firebase no se ocupan entonces se recortan
      res = res.replace("\"","") //el anterior solo quita una de las dos

      usersRef.update({ Contrasena:res }); //se actualiza el dato sin afectar a los demás
      console.log('Your name is', res);

    });
  };


})



app.controller('ReservasCtrl', function($scope,$firebaseArray) {
  console.log(localStorage.getItem("user"));

  $scope.reservas =[];

  var colocarIcono= function ()
  {
    var ref = new Firebase("https://sideappprueba.firebaseio.com/Reserva");

    vehiculos = $firebaseArray(ref);
    vehiculos.$loaded()
      .then(function (data) {
        angular.forEach(data, function (value) {
          console.log(value.Lugar);
          if ( value.Usuario == localStorage.getItem("user")) { // valida que no este reservado
            if(value.Estado == "Pendiente"){
              $scope.colorR = "#FFEA00";
              $scope.icon = "icon ion-android-sync"
              console.log("pendiente");
            }
            else if(value.Estado == "Aceptado"){
              console.log("Aceptado");
              $scope.colorR = "#00E676";
              $scope.icon = "icon ion-android-checkbox-outline"
            }
            else if(value.Estado == "Denegado"){
              console.log("Denegado");
              $scope.colorR = "#F44336";
              $scope.icon = "icon ion-android-close"
            }
            $scope.reservas.push({title: value.Lugar, fechaIni: value.FechaSalida,id:value.$id,color:$scope.colorR,icon:$scope.icon});
          }

        });
      });
  };
  console.log("**mis reservas**");
  colocarIcono();


});





app.controller('VistaMisReservasUsuCtrl', function($scope) {
  var url = window.location.href;
  alert(url);
  var ArregloRes = url.split("/");//cambiar cuando se cambie la url

  res = ArregloRes.pop();
  console.log(res);
  var refRes = new Firebase("https://sideappprueba.firebaseio.com/Reserva/");
  var refUsuario = new Firebase("https://sideappprueba.firebaseio.com/Persona/");

    refRes.child(res).once('value', function(snapshot) {
    console.log(snapshot.val());
    var arrayInfoBD = eval(snapshot.val());                 //almacena el objeto o JSON obtenido de firebase, pero para ser utilzado

    $scope.placa = arrayInfoBD.Placa;
    $scope.chofer = arrayInfoBD.Chofer;
    $scope.lugar = arrayInfoBD.Lugar;
    $scope.hSalida = arrayInfoBD.HoraSalida;
    $scope.hLlegada = arrayInfoBD.HoraLlegada;
    $scope.fSalida = arrayInfoBD.FechaSalida;
    $scope.fLlegada = arrayInfoBD.FechaLlegada;
    $scope.numPas = arrayInfoBD.Pasajeros;
    $scope.estado = arrayInfoBD.Estado;


    });
    var usu = localStorage.getItem("user");
    console.log(usu);
  refUsuario.child(usu).once('value', function(snapshot) {

      var arrayInfoPersona= eval(snapshot.val());                 //almacena el objeto o JSON obtenido de firebase, pero para ser utilzado
      console.log(arrayInfoPersona);
      $scope.nombre = arrayInfoPersona.Nombre+" "+arrayInfoPersona.Apellidos ;

    });
});



app.controller('ReservarTransCtrl', function($scope,$firebaseArray, $window, $ionicPopup,misReservas,$ionicLoading,$timeout) {

  console.log(localStorage.getItem("user"));
  var carRef = new Firebase("https://sideappprueba.firebaseio.com/Vehiculo");
  $scope.data={}; //Objeto donde se guarda todo
  $scope.data.placas = []; // arreglo de placas
  var f = new Date();
  $scope.mindate = f.getFullYear() + "-" + (f.getMonth() +1) + "-" + f.getDate();
  console.log($scope.mindate );



  $scope.$watch('data.placa', function() {
    veriRestVehicular();
  });

  $scope.$watch('data.fechaSalida', function() {
    veriRestVehicular();
  });

  $scope.$watch('data.fechaLlegada', function() {
    veriRestVehicular();
  });



  //PRUEBA
//  var dias_semana = new Array("Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","Sabado");

  //*************************** Funcion para verificar restricción vehicular ***************************
  var veriRestVehicular = function () {

    if($scope.data.placa != undefined && $scope.data.fechaSalida != undefined && $scope.data.fechaLlegada != undefined) {

      var ultimoDPlaca = $scope.data.placa.toString().charAt( $scope.data.placa.toString().length-1);
      var ini = $scope.data.fechaSalida.getDay();
      var fin =  $scope.data.fechaLlegada.getDay();

      var mesini = $scope.data.fechaSalida.getMonth();
      var mesfin = $scope.data.fechaSalida.getMonth();
      var añoini = $scope.data.fechaLlegada .getFullYear();
      var añofin = $scope.data.fechaLlegada .getFullYear();

      console.log(ultimoDPlaca);
      console.log(ini);
      console.log(fin);

      console.log($scope.data.fechaSalida);
      console.log($scope.data.fechaLlegada);

      if(ini >= fin && $scope.data.fechaSalida < undefined && $scope.data.fechaLlegada ){
        $scope.advrRestriccion = "visible";
        console.log("werf");

      }
      else {
        for (var i = ini; i <= fin; i++) {
          if (i == 1 && (ultimoDPlaca == "1" || ultimoDPlaca == "2")) {
            $scope.advrRestriccion = "visible";
            console.log("Res1");
            break;
          }
          else if (i == 2 && (ultimoDPlaca == "3" || ultimoDPlaca == "4")) {
            $scope.advrRestriccion = "visible";
            console.log("Res2");

            break;
          }
          else if (i == 3 && (ultimoDPlaca == "5" || ultimoDPlaca == "6")) {
            $scope.advrRestriccion = "visible";
            console.log("Res3");

            break;
          }
          else if (i == 4 && (ultimoDPlaca == "7" || ultimoDPlaca == "8")) {
            $scope.advrRestriccion = "visible";
            console.log("Res4");

            break;
          }
          else if (i == 5 && (ultimoDPlaca == "9" || ultimoDPlaca == "0")) {
            $scope.advrRestriccion = "visible";
            console.log("Res5");

            break;
          }
          else {
            $scope.advrRestriccion = "hidden";
            console.log("hi");
          }
        }
      }
    }
    else {
      $scope.advrRestriccion = "hidden";
      console.log("hiwer");
    }
  };





  //***********************************************************************************************+


  // *************************** Pone borde gris a todos los inputs***************************
  $scope.bordePlaca = "gray";
  $scope.bordeChofer = "gray";
  $scope.bordeLugar = "gray";
  $scope.bordeHoraSalida = "gray";
  $scope.bordeHoraLlegada = "gray";
  $scope.bordefechaSalida = "gray";
  $scope.bordefechaLlegada = "gray";
  $scope.bordePasajeros = "gray";
  $scope.advrRestriccion = "hidden";



  //*************************** Funcion para actualizar la lista de placas ***************************


  vehiculos = $firebaseArray(carRef);
  //Función que recorre el fireBaseArray y para pasarlo a un array normal y ponerlo en el select
  var cargarPlacas = function ()
  {


    $scope.data.placas=[];
    vehiculos.$loaded()
      .then(function (data) {

        angular.forEach(data, function (value) {
          console.log(value.Placa);
          $scope.data.placas.push(value.Placa);
        });

      });

  };
  //cargarPlacas();
  var url = window.location.href;
  console.log(url);
  var ArregloRes = url.split("/");//cambiar cuando se cambie la url

  var placadeUrl = ArregloRes.pop(); //obtiene el último valor de la url

  if(String(placadeUrl)!="reservar"){
    $scope.data.placa = placadeUrl;
  }
  else{
   cargarPlacas();
  }

  //*************************** Actualizar las placas cada vez q se da click ***************************
  /*
   var listPlacas = document.getElementById("listPlacas");
   if (listPlacas.addEventListener){
   listPlacas.addEventListener("click", cargarPlacas, false);

   }
   else if (listPlacas.attachEvent){
   listPlacas.attachEvent('onclick', cargarPlacas);

   }

   */

  //*****************************Función para reservas***************************************


  $scope.reservar = function() {
    console.log($scope.data.placa);
    // Validación de espacios vacíos
    if ($scope.data.placa != undefined && $scope.data.chofer != undefined && $scope.data.lugar != undefined &&
      $scope.data.horaSalida != undefined && $scope.data.horaLlegada != undefined && $scope.data.fechaSalida != undefined
      && $scope.data.fechaLlegada != undefined && $scope.data.pasajeros != undefined) {

      // VALIDACIONES DE FECHAS Y HORAS

      if ($scope.data.fechaSalida > $scope.data.fechaLlegada) {
        //cambiar a rojo los campos incorrectos
        $scope.bordefechaSalida = "red";
        $scope.bordefechaLlegada = "red";
        // Sehacen gris por aquello de que queden en rojo
        $scope.bordeHoraSalida = "gray";
        $scope.bordeHoraLlegada = "gray";
        $scope.bordeLugar = "gray";
        $scope.bordePasajeros = "gray";
        var alertPopup = $ionicPopup.alert({
          title: 'Fechas inconsistentes',
          template: 'Por favor verifique los datos!!!'
        });
      }

      else if ($scope.data.horaSalida > $scope.data.horaLlegada && $scope.data.fechaSalida.toString() == $scope.data.fechaLlegada.toString()) {
        //cambiar a rojo los campos incorrectos
        $scope.bordeHoraSalida = "red";
        $scope.bordeHoraLlegada = "red";
        // Se hacen gris por aquello de que queden en rojo
        $scope.bordefechaSalida = "gray";
        $scope.bordefechaLlegada = "gray";
        $scope.bordeLugar = "gray";
        $scope.bordePasajeros = "gray";
        var alertPopup = $ionicPopup.alert({
          title: 'Horarios inconsistentes',
          template: 'Por favor verifique los datos!!!'
        });

      }
      else {

        //VERIFICACION DE VEHICULO
        //::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
        /* Validacion de auto*/
        var disponible = true;
        var vehiculo = new Firebase("https://sideappprueba.firebaseio.com/Vehiculo");
        var reservasRealizadas = new Firebase("https://sideappprueba.firebaseio.com/Reserva");
        vehiculo.child($scope.data.placa).once('value', function(snapshot) {
          var arrVehculo = eval(snapshot.val());                 //almacena el objeto o JSON obtenido de firebase, pero para ser utilzado



          var FCIN=  $scope.data.fechaSalida.toLocaleDateString();
          var arrFCIN = FCIN.split('/');
          var fechaIniRes = Date.parse(arrFCIN[1]+"/"+arrFCIN[0]+"/"+arrFCIN[2]);

          var FCFIN=  $scope.data.fechaLlegada.toLocaleDateString();
          var arrFFIN = FCFIN.split('/');
          var fechaFinRes = Date.parse(arrFFIN[1]+"/"+arrFFIN[0]+"/"+arrFFIN[2]);

          if(arrVehculo.Estado == "Reservado" )
          {
            var reservasDeVehRef = new Firebase("https://sideappprueba.firebaseio.com/Vehiculo/"+$scope.data.placa+"/Reservas");
            reservasDeVeh = $firebaseArray(reservasDeVehRef);

            reservasDeVeh.$loaded()
              .then(function (data) {
                var ban = true;
                angular.forEach(data, function (value) {
                  console.log(ban);
                  if(ban == true) {
                    var arrFI = value.FechaSalida.split('/');
                    var arrFF = value.FechaLlegada.split('/');
                    var FI = arrFI[1] + "/" + arrFI[0] + "/" + arrFI[2];
                    var FF = arrFF[1] + "/" + arrFF[0] + "/" + arrFF[2];
                    var dateIni = Date.parse(FI);
                    var dateFin = Date.parse(FF);


                    if ((fechaIniRes >= dateIni && fechaIniRes <= dateFin)|| (fechaFinRes >= dateIni && fechaFinRes <= dateFin )) {//se comparan las fechas
                      var alertPopup = $ionicPopup.alert({
                        title: 'El auto está reservado esa fecha',
                        template: 'Por favor verifique los datos!!!'

                      });
                      ban = false;

                    }
                  }
                });
                console.log(ban);
                if (ban == true){
                  realizarReserva();
                }
              });



           // console.log(arrVehculo.Reservas);

            /*
            for(i;i<arrReservas.length;i++){//Recorrido de las reservas del auto
              reservasRealizadas.child(arrReservas[i]).once('value', function(snapshot2) {
                var reservaActual =  eval(snapshot2.val());//se toma el JSON de casa reserva
                var dateIni = Date.parse(reservaActual.FechaSalida);
                var dateFin = Date.parse(reservaActual.FechaLlegada);
                console.log(dateIni);
                console.log(dateFin);
                console.log(fechaIniRes);
                if( fechaIniRes >= dateIni && fechaIniRes <= dateFin){//se comparan las fechas
                  var alertPopup = $ionicPopup.alert({
                    title: 'El auto está reservado esa fecha',
                    template: 'Por favor verifique los datos!!!'
                  });
                  return;
                }
              });

            }
             */

          }
          //::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
          else {
            realizarReserva();
          }

        });


      }//termina el else de verificacion de fechas y horas
    }//Donde termina el if
    else {
      // Si los datos no estan completos muestra un mensaje
      var alertPopup = $ionicPopup.alert({
        title: 'Datos incompletos',
        template: 'Por favor llene todos los campos!'
      });
      // Verifica que los campos que estan incompletos para poner el borde rojo
      if ($scope.data.placa == undefined) {
        $scope.bordePlaca = "red";
      }
      else {
        $scope.bordePlaca = "gray";
      }

      if ($scope.data.chofer == undefined) {
        $scope.bordeChofer = "red";
      }
      else {
        $scope.bordeChofer = "gray";
      }

      if ($scope.data.lugar == undefined) {
        $scope.bordeLugar = "red";
      }
      else {
        $scope.bordeLugar = "gray";
      }
      if ($scope.data.pasajeros == undefined) {
        $scope.bordePasajeros = "red";
      }
      else {
        $scope.bordePasajeros = "gray";
      }

      if ($scope.data.fechaLlegada == undefined) {
        $scope.bordefechaLlegada = "red";
      }
      else {
        $scope.bordefechaLlegada = "gray";
      }
      if ($scope.data.fechaSalida == undefined) {
        $scope.bordefechaSalida = "red";
      }
      else {
        $scope.bordefechaSalida = "gray";
      }

      if ($scope.data.horaSalida == undefined) {
        $scope.bordeHoraSalida = "red";
      }
      else {
        $scope.bordeHoraSalida = "gray";
      }
      if ($scope.data.horaLlegada == undefined) {
        $scope.bordeHoraLlegada = "red";
      }
      else {
        $scope.bordeHoraLlegada = "gray";
      }
    }
    //___________________________________________________________________________________
  } // Termina la función de reservar

  //FUNCION QUE HACE LA RESERVA SI TODO ESTA BIEN
  function realizarReserva()  {
    //Obtener el id de reserva
    //____________________________________________________________________________________
    //Referencia al numero global para las reservas
    var refNum = new Firebase("https://sideappprueba.firebaseio.com/NumReservasGlobal");
    var refPlaca = new Firebase("https://sideappprueba.firebaseio.com/Vehiculo/" + $scope.data.placa);
    //Función que agrega reservas
    function agregarGlobal(ref) {
      refNum.once('value', function (snapshot) {
        // v es el numero de reserva
        var v = parseInt(snapshot.val());
        //Referencia a la reserva
        var ref = new Firebase("https://sideappprueba.firebaseio.com/Reserva/r-" + v);
        //obtener la referencia del vehiculo escogido para cambiar el atributo estado


        //Agregar reservas
        refPlaca.update({Estado: "Solicitado"});
        ref.update({
          Usuario: localStorage.getItem("user"),
          Placa: $scope.data.placa,
          Chofer: $scope.data.chofer,
          Lugar: $scope.data.lugar,
          HoraSalida: $scope.data.horaSalida.toLocaleTimeString("en-US"),
          HoraLlegada: $scope.data.horaLlegada.toLocaleTimeString("en-US"),
          FechaSalida: $scope.data.fechaSalida.toLocaleDateString(),
          FechaLlegada: $scope.data.fechaLlegada.toLocaleDateString(),
          Pasajeros: $scope.data.pasajeros,
          //ESTADOS : Pendeinte, Aceptado, Denegado
          Estado: "Pendiente",
          Justificacion: " "
        });

        //Agregar el elemnto a la lista
        misReservas.push({title: $scope.data.lugar, fechaIni:$scope.data.fechaSalida.toLocaleDateString(),id: "r-"+ v})




        cargarPlacas();//Actualiza las placas
        // Actualizar la variable global que lleva el ID  de las reservas
        var refActualizar = new Firebase("https://sideappprueba.firebaseio.com");
        v = parseInt(v) + 1;
        refActualizar.update({NumReservasGlobal: v});


        $scope.showAlert = function () {
          $ionicPopup.alert({
            title: 'Reservado!',
            content: 'Se ha reservado el auto: ' + $scope.data.placa
            //Limpiar los campos
          }).then(function (res) {
            $scope.data.placa = undefined;
            $scope.data.chofer = undefined;
            $scope.data.lugar = undefined;
            $scope.data.horaSalida = undefined;
            $scope.data.horaLlegada= undefined;
            $scope.data.fechaSalida= undefined;
            $scope.data.fechaLlegada= undefined;
            $scope.data.pasajeros= undefined;

            // *************************** Pone borde gris a todos los inputs***************************
            $scope.bordePlaca = "gray";
            $scope.bordeChofer = "gray";
            $scope.bordeLugar = "gray";
            $scope.bordeHoraSalida = "gray";
            $scope.bordeHoraLlegada = "gray";
            $scope.bordefechaSalida = "gray";
            $scope.bordefechaLlegada = "gray";
            $scope.bordePasajeros = "gray";
          });
        };

        $scope.showAlert();

      });
    }
    // LLamada a la función
    agregarGlobal(refNum, function (val) {
    });

  }


}); // Termina el controller


app.controller('FlotillasCtrl', function($scope,$firebaseArray,$state) {
  var autosList = [];
  $scope.cargarReservas= function ()
  {
    autosList=[];

    var ref = new Firebase("https://sideappprueba.firebaseio.com/Vehiculo");
    vehiculos = $firebaseArray(ref);
    vehiculos.$loaded()
      .then(function (data) {
        angular.forEach(data, function (value) {
          autosList.push({placa: value.Placa, estado: value.Estado, id:value.$id, image:value.image, marca:value.Marca});
          if(value.Estado =="Aceptado" || value.estado == "Denegado"){
            $scope.buttonvisible = false;
          }else{
            $scope.buttonvisible = true;
          }
        });
      }).finally(function() {
      // Stop the ion-refresher from spinning
      $scope.$broadcast('scroll.refreshComplete');
    });

    $scope.auto =autosList;

  };
  $scope.cargarReservas();
  $scope.AgregarVehiculo =function () {
     $state.go("app.AddVehic");
  }

});

app.controller('flotillaDet', function($scope,$firebaseArray,$cordovaCamera, $ionicPopover,ActualizarVehi,$state) {
  var usersRef = new Firebase('https://sideappprueba.firebaseio.com/Vehiculo');

  var url = window.location.href;
  var arreglo = url.split("/");//cambiar cuando se cambie la url
  var placa = arreglo.pop();  //obtiene la placa del URL y gestiona una búsqueda en firebase para obtener la información

  console.log(placa);

  $scope.placasD = "";
  $scope.traccionD =  "";
  $scope.pasajerosD = "";
  $scope.dependenciaD = "";
  $scope.estadosD =  "";
  $scope.imagenD = "";
  $scope.marcaD =  "";
  $scope.modeloD = "";

  usersRef.child(placa).once('value', function (snapshot) {
    console.log(snapshot.val());
    var exists = (snapshot.val() !== null);
    var arrayInfoBD = eval(snapshot.val());
    $scope.placasD = arrayInfoBD.Placa;
    $scope.traccionD =  arrayInfoBD.Traccion;
    $scope.pasajerosD = arrayInfoBD.Capacidad;
    $scope.dependenciaD = arrayInfoBD.Dependencia;
    $scope.estadosD =  arrayInfoBD.Estado;
    $scope.imagenD = arrayInfoBD.image;
    $scope.marcaD =  arrayInfoBD.Marca;
    $scope.modeloD = arrayInfoBD.Modelo;
  });

    var userReference = new Firebase("https://sideappprueba.firebaseio.com/Vehiculo/" + placa);


    $scope.upload = function(typef) {
      var typeofpic;
      if(typef === "camera"){
        typeofpic = Camera.PictureSourceType.CAMERA;
      }
      else if(typef ==="gallery"){
        typeofpic =  Camera.PictureSourceType.SAVEDPHOTOALBUM;
      }
      var options = {
        quality : 75,
        destinationType : Camera.DestinationType.DATA_URL,
        sourceType : typeofpic,
        allowEdit : true,
        encodingType: Camera.EncodingType.JPEG,
        popoverOptions: CameraPopoverOptions,
        targetWidth: 1500,
        targetHeight: 1000,
        saveToPhotoAlbum: false
      };
      $cordovaCamera.getPicture(options).then(function(imageData) {
        userReference.update({image: imageData}).then(function() {
          alert("Image has been uploaded");
        });
      }, function(error) {
        console.error(error);
      });


      //controlador para el menu contextual con las opciones de tomar video o usar imagenes de galería




    }
    $scope.updateData = function (marcaD,modeloD,traccionD,pasajerosD,dependenciaD) {
      var data = {ID:placa,marca:marcaD,modelo:modeloD,traccion:traccionD,capacidad:pasajerosD,dependencia:dependenciaD};
      ActualizarVehi.AtVehiculo(data);
    }

  $scope.BorrarVehic = function () {
    var data = {ID:placa};
    ActualizarVehi.BorraVehiculo(data);
    $state.go("appAdmin.flotilla");

  }

  $ionicPopover.fromTemplateUrl('templates/popover.html', {
    scope: $scope,
  }).then(function(popover) {
    $scope.popover = popover;
  });




});

app.controller('AddVehic',function (ActualizarVehi,$scope,$state) {
  $scope.addVehic =function (placasD,imagenD,marcaD,modeloD,traccionD,pasajerosD,dependenciaD) {
    var data = {ID:placasD,marca:marcaD,modelo:modeloD,traccion:traccionD,capacidad:pasajerosD,dependencia:dependenciaD,placa:placasD};
    ActualizarVehi.AtVehiculo(data);
    $state.go("appAdmin.flotilla");
  }
  $scope.BorrarVehic = function () {
    $state.go("appAdmin.flotilla");
  }
})

app.controller('AdminResevasCtrl', function($scope,$firebaseArray) {



  $scope.dorefresh = function()
  {
    $scope.groups = [];
    $scope.groups[1] = {
      name: 'Aceptados',
      items: []
    };
    $scope.groups[2] = {
      name: 'Denegados',
      items: []
    };
    $scope.groups[0] = {
      name: 'Pendientes',
      items: []
    };

    var ref = new Firebase("https://sideappprueba.firebaseio.com/Reserva");

    vehiculos = $firebaseArray(ref);
    vehiculos.$loaded()
      .then(function (data) {
        angular.forEach(data, function (value) {
          console.log(value.Lugar);

          if (value.Estado == "Pendiente") {
            $scope.colorR = "#FFEA00";
            $scope.icon = "icon ion-android-sync"
            console.log("pendiente");
            $scope.groups[0].items.push({
              title: value.Lugar,
              fechaIni: value.FechaSalida,
              id: value.$id,
              color: $scope.colorR,
              icon: $scope.icon
            });
          }
          else if (value.Estado == "Aceptado") {
            console.log("Aceptado");
            $scope.colorR = "#00E676";
            $scope.icon = "icon ion-android-checkbox-outline"
            $scope.groups[1].items.push({
              title: value.Lugar,
              fechaIni: value.FechaSalida,
              id: value.$id,
              color: $scope.colorR,
              icon: $scope.icon
            });
          }
          else if (value.Estado == "Denegado") {
            console.log("Denegado");
            $scope.colorR = "#F44336";
            $scope.icon = "icon ion-android-close"
            $scope.groups[2].items.push({
              title: value.Lugar,
              fechaIni: value.FechaSalida,
              id: value.$id,
              color: $scope.colorR,
              icon: $scope.icon
            });
          }

        });
      }).finally(function () {
        $scope.$broadcast('scroll.refreshComplete')
    });
  };
    $scope.dorefresh();
    /*
     * if given group is the selected group, deselect it
     * else, select the given group
     */
    $scope.toggleGroup = function(group) {
      if ($scope.isGroupShown(group)) {
        $scope.shownGroup = null;
      } else {
        $scope.shownGroup = group;
      }
    };
    $scope.isGroupShown = function(group) {
      return $scope.shownGroup === group;
    };


});





app.controller('VistaMisReservasAdminCtrl', function($scope,$firebaseArray,EvaluarReserva,$ionicPopup,$state) {

  var url = window.location.href;
  var ArregloRes = url.split("/");//cambiar cuando se cambie la url

  res = ArregloRes.pop();
  console.log(res);
  var refRes = new Firebase("https://sideappprueba.firebaseio.com/Reserva/");
  var refUsuario = new Firebase("https://sideappprueba.firebaseio.com/Persona/");

  refRes.child(res).once('value', function(snapshot) {
    console.log(snapshot.val());
    var arrayInfoBD = eval(snapshot.val());                 //almacena el objeto o JSON obtenido de firebase, pero para ser utilzado

    var chofRef = new Firebase("https://sideappprueba.firebaseio.com/Choferes");
    chofs = $firebaseArray(chofRef);




    //CARGAR CHOFERES EN LA LISTA
    var cargarChofer = function ()
    {
      $scope.chofers=[];
      if( arrayInfoBD.Chofer == 'No'){
        console.log(arrayInfoBD.Chofer );
        $scope.chofers.push("No");
        $scope.chofer = $scope.chofers[0];
      }

      else {
        if (arrayInfoBD.Chofer == 'Si') {
          $scope.chofers.push("-- Seleccione --");
          chofs.$loaded()
            .then(function (data) {

              angular.forEach(data, function (value) {
                console.log(value.Nombre);
                $scope.chofers.push(value.Nombre + ' ' + value.Apellidos);
              });

            });
          $scope.chofer = $scope.chofers[0];

        }
        else {


          chofs.$loaded()
            .then(function (data) {

              angular.forEach(data, function (value) {
                console.log(value.Nombre);
                $scope.chofers.push(value.Nombre + ' ' + value.Apellidos);
              });
              var i = 0;
              console.log($scope.chofers.length);
              for(i;i< $scope.chofers.length;i++){
                console.log("sd");
                if(arrayInfoBD.Chofer == $scope.chofers[i]){
                  $scope.chofer = $scope.chofers[i];
                }
              }
            });

        }
      }
    };
    cargarChofer();

    //CRAGAR OTROS ELEMENTOS
    $scope.placa = arrayInfoBD.Placa;
    $scope.lugar = arrayInfoBD.Lugar;
    $scope.hSalida = arrayInfoBD.HoraSalida;
    $scope.hLlegada = arrayInfoBD.HoraLlegada;
    $scope.fSalida = arrayInfoBD.FechaSalida;
    $scope.fLlegada = arrayInfoBD.FechaLlegada;
    $scope.numPas = arrayInfoBD.Pasajeros;
    $scope.estado = arrayInfoBD.Estado;

    //CARGAR MENSAJES DE BOTON
    if( $scope.estado == "Pendiente"){
      $scope.estadoBotonAceptar = "Aceptar";
      $scope.estadoBotonDenegar = "Denegar";

    }
    else if($scope.estado == "Aceptado")
    {
      $scope.estadoBotonAceptar = "Editar";
      $scope.estadoBotonDenegar = "Denegar";
    }
    else if($scope.estado == "Denegado")
    {
      $scope.estadoBotonAceptar = "Aceptar";
      $scope.estadoBotonDenegar = "Editar";
    }
    //CARGAR JUSTIFICACIÓN

<<<<<<< HEAD
    if(arrayInfoBD.Justificacion != " "){
=======
    if(arrayInfoBD.Justificacion != ""){
>>>>>>> 0d6b88caf3074c3e2ac30cb86572a69c90c1a125
      $scope.justificacionRechazo = arrayInfoBD.Justificacion;
    }

    refUsuario.child(arrayInfoBD.Usuario).once('value', function(snapshot) {

      var arrayInfoBDP= eval(snapshot.val());                 //almacena el objeto o JSON obtenido de firebase, pero para ser utilzado
      console.log(arrayInfoBDP);
      $scope.nombrePersonaReserva = arrayInfoBDP.Nombre+" "+arrayInfoBDP.Apellidos ;

    });


  });
  var usu = localStorage.getItem("user");
  console.log(usu);
  refUsuario.child(usu).once('value', function(snapshot) {

    var arrayInfoPersona= eval(snapshot.val());                 //almacena el objeto o JSON obtenido de firebase, pero para ser utilzado
    console.log(arrayInfoPersona);
    $scope.nombre = arrayInfoPersona.Nombre+" "+arrayInfoPersona.Apellidos ;

  });


  $scope.evaluar = function (estado, justificacion,chofer) {
    var data;
    if(estado == "Denegado"){
      if(justificacion == undefined){
        var alertPopup = $ionicPopup.alert({
          title: 'Error',
          template: 'Por favor indique una Justificación!'
        });
      }else{
        console.log(justificacion);
        data ={ID:res,Estado:estado,Justificacion:justificacion,Chofer:"Si"};
        EvaluarReserva.setEstado(data);

      }
    }
    else if(estado == "Aceptado"){
      if(chofer == '-- Seleccione --'){
        var alertPopup = $ionicPopup.alert({
          title: 'Error',
          template: 'Por favor indique una chofer!'
        });
      }
      else {
        data = {ID: res, Estado: estado, Justificacion: "",Chofer:chofer};
        EvaluarReserva.setEstado(data);
<<<<<<< HEAD
=======
        $state.go('appAdmin.adminReservas');
>>>>>>> 0d6b88caf3074c3e2ac30cb86572a69c90c1a125
      }
    }

  }

});



app.controller('controlChoferes', function($scope,ConsultaChoferes) {
  ConsultaChoferes.consultarChoferes(function (data) {

    $scope.choferesData = data;

  });


});
