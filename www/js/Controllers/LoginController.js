/**
 * Created by Hellen Rojas Rojas on 03/08/2016.
 */

app.controller('LoginCtrl', function($scope, LoginService, $ionicPopup, $firebaseArray, $state) {
  $scope.data = {};


  $scope.login = function() {
    LoginService.loginUser($scope.data.username, $scope.data.password).success(function(data) {
      //$state.go('appAdmin.inicioAdmin');
      if(data == "Administrador"){
        localStorage.setItem("user", $scope.data.username);
        $state.go('appAdmin.inicioAdmin');
        console.log("Admin");
      }else{
        localStorage.setItem("user", $scope.data.username);
        $state.go('app.inicio');
        console.log("User");
      }




    }).error(function(data) {
      var alertPopup = $ionicPopup.alert({
        title: 'Logeo Fallido!',
        template: 'Por favor verifique sus credenciales!'
      });
    });
  }



})
