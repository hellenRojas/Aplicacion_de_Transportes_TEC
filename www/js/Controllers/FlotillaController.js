/**
 * Created by Hellen Rojas Rojas on 03/08/2016.
 */


app.controller('FlotillasCtrl', function($scope,$firebaseArray,$state) {
  var autosList = [];
  $scope.cargarReservas= function ()
  {
    autosList=[];

    var ref = new Firebase("https://sideappprueba.firebaseio.com/Vehiculo");
    vehiculos = $firebaseArray(ref);
    vehiculos.$loaded()
      .then(function (data) {
        angular.forEach(data, function (value) {
          autosList.push({placa: value.Placa, estado: value.Estado, id:value.$id, image:value.image, marca:value.Marca});
          if(value.Estado =="Aceptado" || value.estado == "Denegado"){
            $scope.buttonvisible = false;
          }else{
            $scope.buttonvisible = true;
          }
        });
      }).finally(function() {
      // Stop the ion-refresher from spinning
      $scope.$broadcast('scroll.refreshComplete');
    });

    $scope.auto =autosList;

  };
  $scope.cargarReservas();
  $scope.AgregarVehiculo =function () {
    $state.go("app.AddVehic");
  }

});
