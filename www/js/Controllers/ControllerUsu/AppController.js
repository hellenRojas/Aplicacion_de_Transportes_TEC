/**
 * Created by Hellen Rojas Rojas on 03/08/2016.
 */

app.controller('AppCtrl', function($scope, $ionicModal, $timeout, $ionicLoading, $state, $ionicPopup) {

  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //$scope.$on('$ionicView.enter', function(e) {
  //});
  $scope.logOut = function(){
    ionic.Platform.exitApp()
  };



  // Form data for the login modal
  $scope.loginData = {};

  // Create the login modal that we will use later
  $ionicModal.fromTemplateUrl('templates/login.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modal = modal;
  });

  // Triggered in the login modal to close it
  $scope.closeLogin = function() {
    $scope.modal.hide();
  };

  // Open the login modal
  $scope.login = function() {
    $scope.modal.show();
  };

  // Perform the login action when the user submits the login form
  $scope.doLogin = function() {
    console.log('Doing login', $scope.loginData);

    // Simulate a login delay. Remove this and replace with your login
    // code if using a login system
    $timeout(function() {
      $scope.closeLogin();
    }, 1000);
  };

  //POPUP PARA CAMBIAR CONTRASEÑA DE USUARIOS


  var user = localStorage.getItem("user"); //obtiene la variable de sessión del cookie, donde tiene el usuario
  user = user.replace("\"","") //el cookie devuelve el user con comillas y para firebase no se ocupan entonces se recortan
  user = user.replace("\"","") //el anterior solo quita una de las dos

  var USERS_LOCATION = 'https://sideappprueba.firebaseio.com/Persona/'+user; //se completa el url de firebase con el user obtenido

  var usersRef = new Firebase(USERS_LOCATION);

  //GENERAR EL POPUP DEL CAMBIAR CONTRASEÑA
  // When button is clicked, the popup will be shown...
  $scope.showPrompt = function() {
    $ionicPopup.prompt({
      title: 'Cambiar Contraseña',
    }).then(function(res) {
      res = res.replace("\"","") //el cookie devuelve el user con comillas y para firebase no se ocupan entonces se recortan
      res = res.replace("\"","") //el anterior solo quita una de las dos

      usersRef.update({ Contrasena:res }); //se actualiza el dato sin afectar a los demás
      console.log('Your name is', res);

    });
  };


})
