/**
 * Created by Hellen Rojas Rojas on 03/08/2016.
 */


app.controller('ReservasCtrl', function($scope,$firebaseArray) {
  console.log(localStorage.getItem("user"));

  $scope.reservas =[];

  var colocarIcono= function ()
  {
    var ref = new Firebase("https://sideappprueba.firebaseio.com/Reserva");

    vehiculos = $firebaseArray(ref);
    vehiculos.$loaded()
      .then(function (data) {
        angular.forEach(data, function (value) {
          console.log(value.Lugar);
          if ( value.Usuario == localStorage.getItem("user")) { // valida que no este reservado
            if(value.Estado == "Pendiente"){
              $scope.colorR = "#FFEA00";
              $scope.icon = "icon ion-android-sync"
              console.log("pendiente");
            }
            else if(value.Estado == "Aceptado"){
              console.log("Aceptado");
              $scope.colorR = "#00E676";
              $scope.icon = "icon ion-android-checkbox-outline"
            }
            else if(value.Estado == "Denegado"){
              console.log("Denegado");
              $scope.colorR = "#F44336";
              $scope.icon = "icon ion-android-close"
            }
            $scope.reservas.push({title: value.Lugar, fechaIni: value.FechaSalida,id:value.$id,color:$scope.colorR,icon:$scope.icon});
          }

        });
      });
  };
  console.log("**mis reservas**");
  colocarIcono();


});
