/**
 * Created by Hellen Rojas Rojas on 03/08/2016.
 */

app.controller('ReservarTransCtrl', function($scope,$firebaseArray, $window, $ionicPopup,misReservas,$ionicLoading,$timeout) {

  console.log(localStorage.getItem("user"));
  var carRef = new Firebase("https://sideappprueba.firebaseio.com/Vehiculo");
  $scope.data={}; //Objeto donde se guarda todo
  $scope.data.placas = []; // arreglo de placas
  var f = new Date();
  $scope.mindate = f.getFullYear() + "-" + (f.getMonth() +1) + "-" + f.getDate();
  console.log($scope.mindate );



  $scope.$watch('data.placa', function() {
    veriRestVehicular();
  });

  $scope.$watch('data.fechaSalida', function() {
    veriRestVehicular();
  });

  $scope.$watch('data.fechaLlegada', function() {
    veriRestVehicular();
  });



  //PRUEBA
//  var dias_semana = new Array("Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","Sabado");

  //*************************** Funcion para verificar restricción vehicular ***************************
  var veriRestVehicular = function () {

    if($scope.data.placa != undefined && $scope.data.fechaSalida != undefined && $scope.data.fechaLlegada != undefined) {

      var ultimoDPlaca = $scope.data.placa.toString().charAt( $scope.data.placa.toString().length-1);
      var ini = $scope.data.fechaSalida.getDay();
      var fin =  $scope.data.fechaLlegada.getDay();

      var mesini = $scope.data.fechaSalida.getMonth();
      var mesfin = $scope.data.fechaSalida.getMonth();
      var añoini = $scope.data.fechaLlegada .getFullYear();
      var añofin = $scope.data.fechaLlegada .getFullYear();

      console.log(ultimoDPlaca);
      console.log(ini);
      console.log(fin);

      console.log($scope.data.fechaSalida);
      console.log($scope.data.fechaLlegada);

      if(ini >= fin && $scope.data.fechaSalida < undefined && $scope.data.fechaLlegada ){
        $scope.advrRestriccion = "visible";
        console.log("werf");

      }
      else {
        for (var i = ini; i <= fin; i++) {
          if (i == 1 && (ultimoDPlaca == "1" || ultimoDPlaca == "2")) {
            $scope.advrRestriccion = "visible";
            console.log("Res1");
            break;
          }
          else if (i == 2 && (ultimoDPlaca == "3" || ultimoDPlaca == "4")) {
            $scope.advrRestriccion = "visible";
            console.log("Res2");

            break;
          }
          else if (i == 3 && (ultimoDPlaca == "5" || ultimoDPlaca == "6")) {
            $scope.advrRestriccion = "visible";
            console.log("Res3");

            break;
          }
          else if (i == 4 && (ultimoDPlaca == "7" || ultimoDPlaca == "8")) {
            $scope.advrRestriccion = "visible";
            console.log("Res4");

            break;
          }
          else if (i == 5 && (ultimoDPlaca == "9" || ultimoDPlaca == "0")) {
            $scope.advrRestriccion = "visible";
            console.log("Res5");

            break;
          }
          else {
            $scope.advrRestriccion = "hidden";
            console.log("hi");
          }
        }
      }
    }
    else {
      $scope.advrRestriccion = "hidden";
      console.log("hiwer");
    }
  };





  //***********************************************************************************************+


  // *************************** Pone borde gris a todos los inputs***************************
  $scope.bordePlaca = "gray";
  $scope.bordeChofer = "gray";
  $scope.bordeLugar = "gray";
  $scope.bordeHoraSalida = "gray";
  $scope.bordeHoraLlegada = "gray";
  $scope.bordefechaSalida = "gray";
  $scope.bordefechaLlegada = "gray";
  $scope.bordePasajeros = "gray";
  $scope.advrRestriccion = "hidden";



  //*************************** Funcion para actualizar la lista de placas ***************************


  vehiculos = $firebaseArray(carRef);
  //Función que recorre el fireBaseArray y para pasarlo a un array normal y ponerlo en el select
  var cargarPlacas = function ()
  {


    $scope.data.placas=[];
    vehiculos.$loaded()
      .then(function (data) {

        angular.forEach(data, function (value) {
          console.log(value.Placa);
          $scope.data.placas.push(value.Placa);
        });

      });

  };
  //cargarPlacas();
  var url = window.location.href;
  console.log(url);
  var ArregloRes = url.split("/");//cambiar cuando se cambie la url

  var placadeUrl = ArregloRes.pop(); //obtiene el último valor de la url

  if(String(placadeUrl)!="reservar"){
    $scope.data.placa = placadeUrl;
  }
  else{
    cargarPlacas();
  }

  //*************************** Actualizar las placas cada vez q se da click ***************************
  /*
   var listPlacas = document.getElementById("listPlacas");
   if (listPlacas.addEventListener){
   listPlacas.addEventListener("click", cargarPlacas, false);

   }
   else if (listPlacas.attachEvent){
   listPlacas.attachEvent('onclick', cargarPlacas);

   }

   */

  //*****************************Función para reservas***************************************


  $scope.reservar = function() {
    console.log($scope.data.placa);
    // Validación de espacios vacíos
    if ($scope.data.placa != undefined && $scope.data.chofer != undefined && $scope.data.lugar != undefined &&
      $scope.data.horaSalida != undefined && $scope.data.horaLlegada != undefined && $scope.data.fechaSalida != undefined
      && $scope.data.fechaLlegada != undefined && $scope.data.pasajeros != undefined) {

      // VALIDACIONES DE FECHAS Y HORAS

      if ($scope.data.fechaSalida > $scope.data.fechaLlegada) {
        //cambiar a rojo los campos incorrectos
        $scope.bordefechaSalida = "red";
        $scope.bordefechaLlegada = "red";
        // Sehacen gris por aquello de que queden en rojo
        $scope.bordeHoraSalida = "gray";
        $scope.bordeHoraLlegada = "gray";
        $scope.bordeLugar = "gray";
        $scope.bordePasajeros = "gray";
        var alertPopup = $ionicPopup.alert({
          title: 'Fechas inconsistentes',
          template: 'Por favor verifique los datos!!!'
        });
      }

      else if ($scope.data.horaSalida > $scope.data.horaLlegada && $scope.data.fechaSalida.toString() == $scope.data.fechaLlegada.toString()) {
        //cambiar a rojo los campos incorrectos
        $scope.bordeHoraSalida = "red";
        $scope.bordeHoraLlegada = "red";
        // Se hacen gris por aquello de que queden en rojo
        $scope.bordefechaSalida = "gray";
        $scope.bordefechaLlegada = "gray";
        $scope.bordeLugar = "gray";
        $scope.bordePasajeros = "gray";
        var alertPopup = $ionicPopup.alert({
          title: 'Horarios inconsistentes',
          template: 'Por favor verifique los datos!!!'
        });

      }
      else {

        //VERIFICACION DE VEHICULO
        //::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
        /* Validacion de auto*/
        var disponible = true;
        var vehiculo = new Firebase("https://sideappprueba.firebaseio.com/Vehiculo");
        var reservasRealizadas = new Firebase("https://sideappprueba.firebaseio.com/Reserva");
        vehiculo.child($scope.data.placa).once('value', function(snapshot) {
          var arrVehculo = eval(snapshot.val());                 //almacena el objeto o JSON obtenido de firebase, pero para ser utilzado



          var FCIN=  $scope.data.fechaSalida.toLocaleDateString();
          var arrFCIN = FCIN.split('/');
          var fechaIniRes = Date.parse(arrFCIN[1]+"/"+arrFCIN[0]+"/"+arrFCIN[2]);

          var FCFIN=  $scope.data.fechaLlegada.toLocaleDateString();
          var arrFFIN = FCFIN.split('/');
          var fechaFinRes = Date.parse(arrFFIN[1]+"/"+arrFFIN[0]+"/"+arrFFIN[2]);

          if(arrVehculo.Estado == "Reservado" )
          {
            var reservasDeVehRef = new Firebase("https://sideappprueba.firebaseio.com/Vehiculo/"+$scope.data.placa+"/Reservas");
            reservasDeVeh = $firebaseArray(reservasDeVehRef);

            reservasDeVeh.$loaded()
              .then(function (data) {
                var ban = true;
                angular.forEach(data, function (value) {
                  console.log(ban);
                  if(ban == true) {
                    var arrFI = value.FechaSalida.split('/');
                    var arrFF = value.FechaLlegada.split('/');
                    var FI = arrFI[1] + "/" + arrFI[0] + "/" + arrFI[2];
                    var FF = arrFF[1] + "/" + arrFF[0] + "/" + arrFF[2];
                    var dateIni = Date.parse(FI);
                    var dateFin = Date.parse(FF);


                    if ((fechaIniRes >= dateIni && fechaIniRes <= dateFin)|| (fechaFinRes >= dateIni && fechaFinRes <= dateFin )) {//se comparan las fechas
                      var alertPopup = $ionicPopup.alert({
                        title: 'El auto está reservado esa fecha',
                        template: 'Por favor verifique los datos!!!'

                      });
                      ban = false;

                    }
                  }
                });
                console.log(ban);
                if (ban == true){
                  realizarReserva();
                }
              });



            // console.log(arrVehculo.Reservas);

            /*
             for(i;i<arrReservas.length;i++){//Recorrido de las reservas del auto
             reservasRealizadas.child(arrReservas[i]).once('value', function(snapshot2) {
             var reservaActual =  eval(snapshot2.val());//se toma el JSON de casa reserva
             var dateIni = Date.parse(reservaActual.FechaSalida);
             var dateFin = Date.parse(reservaActual.FechaLlegada);
             console.log(dateIni);
             console.log(dateFin);
             console.log(fechaIniRes);
             if( fechaIniRes >= dateIni && fechaIniRes <= dateFin){//se comparan las fechas
             var alertPopup = $ionicPopup.alert({
             title: 'El auto está reservado esa fecha',
             template: 'Por favor verifique los datos!!!'
             });
             return;
             }
             });

             }
             */

          }
          //::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
          else {
            realizarReserva();
          }

        });


      }//termina el else de verificacion de fechas y horas
    }//Donde termina el if
    else {
      // Si los datos no estan completos muestra un mensaje
      var alertPopup = $ionicPopup.alert({
        title: 'Datos incompletos',
        template: 'Por favor llene todos los campos!'
      });
      // Verifica que los campos que estan incompletos para poner el borde rojo
      if ($scope.data.placa == undefined) {
        $scope.bordePlaca = "red";
      }
      else {
        $scope.bordePlaca = "gray";
      }

      if ($scope.data.chofer == undefined) {
        $scope.bordeChofer = "red";
      }
      else {
        $scope.bordeChofer = "gray";
      }

      if ($scope.data.lugar == undefined) {
        $scope.bordeLugar = "red";
      }
      else {
        $scope.bordeLugar = "gray";
      }
      if ($scope.data.pasajeros == undefined) {
        $scope.bordePasajeros = "red";
      }
      else {
        $scope.bordePasajeros = "gray";
      }

      if ($scope.data.fechaLlegada == undefined) {
        $scope.bordefechaLlegada = "red";
      }
      else {
        $scope.bordefechaLlegada = "gray";
      }
      if ($scope.data.fechaSalida == undefined) {
        $scope.bordefechaSalida = "red";
      }
      else {
        $scope.bordefechaSalida = "gray";
      }

      if ($scope.data.horaSalida == undefined) {
        $scope.bordeHoraSalida = "red";
      }
      else {
        $scope.bordeHoraSalida = "gray";
      }
      if ($scope.data.horaLlegada == undefined) {
        $scope.bordeHoraLlegada = "red";
      }
      else {
        $scope.bordeHoraLlegada = "gray";
      }
    }
    //___________________________________________________________________________________
  } // Termina la función de reservar

  //FUNCION QUE HACE LA RESERVA SI TODO ESTA BIEN
  function realizarReserva()  {
    //Obtener el id de reserva
    //____________________________________________________________________________________
    //Referencia al numero global para las reservas
    var refNum = new Firebase("https://sideappprueba.firebaseio.com/NumReservasGlobal");
    var refPlaca = new Firebase("https://sideappprueba.firebaseio.com/Vehiculo/" + $scope.data.placa);
    //Función que agrega reservas
    function agregarGlobal(ref) {
      refNum.once('value', function (snapshot) {
        // v es el numero de reserva
        var v = parseInt(snapshot.val());
        //Referencia a la reserva
        var ref = new Firebase("https://sideappprueba.firebaseio.com/Reserva/r-" + v);
        //obtener la referencia del vehiculo escogido para cambiar el atributo estado


        //Agregar reservas
        refPlaca.update({Estado: "Solicitado"});
        ref.update({
          Usuario: localStorage.getItem("user"),
          Placa: $scope.data.placa,
          Chofer: $scope.data.chofer,
          Lugar: $scope.data.lugar,
          HoraSalida: $scope.data.horaSalida.toLocaleTimeString("en-US"),
          HoraLlegada: $scope.data.horaLlegada.toLocaleTimeString("en-US"),
          FechaSalida: $scope.data.fechaSalida.toLocaleDateString(),
          FechaLlegada: $scope.data.fechaLlegada.toLocaleDateString(),
          Pasajeros: $scope.data.pasajeros,
          //ESTADOS : Pendeinte, Aceptado, Denegado
          Estado: "Pendiente",
          Justificacion: " "
        });

        //Agregar el elemnto a la lista
        misReservas.push({title: $scope.data.lugar, fechaIni:$scope.data.fechaSalida.toLocaleDateString(),id: "r-"+ v})




        cargarPlacas();//Actualiza las placas
        // Actualizar la variable global que lleva el ID  de las reservas
        var refActualizar = new Firebase("https://sideappprueba.firebaseio.com");
        v = parseInt(v) + 1;
        refActualizar.update({NumReservasGlobal: v});


        $scope.showAlert = function () {
          $ionicPopup.alert({
            title: 'Reservado!',
            content: 'Se ha reservado el auto: ' + $scope.data.placa
            //Limpiar los campos
          }).then(function (res) {
            $scope.data.placa = undefined;
            $scope.data.chofer = undefined;
            $scope.data.lugar = undefined;
            $scope.data.horaSalida = undefined;
            $scope.data.horaLlegada= undefined;
            $scope.data.fechaSalida= undefined;
            $scope.data.fechaLlegada= undefined;
            $scope.data.pasajeros= undefined;

            // *************************** Pone borde gris a todos los inputs***************************
            $scope.bordePlaca = "gray";
            $scope.bordeChofer = "gray";
            $scope.bordeLugar = "gray";
            $scope.bordeHoraSalida = "gray";
            $scope.bordeHoraLlegada = "gray";
            $scope.bordefechaSalida = "gray";
            $scope.bordefechaLlegada = "gray";
            $scope.bordePasajeros = "gray";
          });
        };

        $scope.showAlert();

      });
    }
    // LLamada a la función
    agregarGlobal(refNum, function (val) {
    });

  }


}); // Termina el controller

