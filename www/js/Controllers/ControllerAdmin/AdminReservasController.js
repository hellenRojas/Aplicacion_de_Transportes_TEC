/**
 * Created by Hellen Rojas Rojas on 03/08/2016.
 */
app.controller('AdminResevasCtrl', function($scope,$firebaseArray) {



  $scope.dorefresh = function()
  {
    $scope.groups = [];
    $scope.groups[1] = {
      name: 'Aceptados',
      items: []
    };
    $scope.groups[2] = {
      name: 'Denegados',
      items: []
    };
    $scope.groups[0] = {
      name: 'Pendientes',
      items: []
    };

    var ref = new Firebase("https://sideappprueba.firebaseio.com/Reserva");

    vehiculos = $firebaseArray(ref);
    vehiculos.$loaded()
      .then(function (data) {
        angular.forEach(data, function (value) {
          console.log(value.Lugar);

          if (value.Estado == "Pendiente") {
            $scope.colorR = "#FFEA00";
            $scope.icon = "icon ion-android-sync"
            console.log("pendiente");
            $scope.groups[0].items.push({
              title: value.Lugar,
              fechaIni: value.FechaSalida,
              id: value.$id,
              color: $scope.colorR,
              icon: $scope.icon
            });
          }
          else if (value.Estado == "Aceptado") {
            console.log("Aceptado");
            $scope.colorR = "#00E676";
            $scope.icon = "icon ion-android-checkbox-outline"
            $scope.groups[1].items.push({
              title: value.Lugar,
              fechaIni: value.FechaSalida,
              id: value.$id,
              color: $scope.colorR,
              icon: $scope.icon
            });
          }
          else if (value.Estado == "Denegado") {
            console.log("Denegado");
            $scope.colorR = "#F44336";
            $scope.icon = "icon ion-android-close"
            $scope.groups[2].items.push({
              title: value.Lugar,
              fechaIni: value.FechaSalida,
              id: value.$id,
              color: $scope.colorR,
              icon: $scope.icon
            });
          }

        });
      }).finally(function () {
      $scope.$broadcast('scroll.refreshComplete')
    });
  };
  $scope.dorefresh();
  /*
   * if given group is the selected group, deselect it
   * else, select the given group
   */
  $scope.toggleGroup = function(group) {
    if ($scope.isGroupShown(group)) {
      $scope.shownGroup = null;
    } else {
      $scope.shownGroup = group;
    }
  };
  $scope.isGroupShown = function(group) {
    return $scope.shownGroup === group;
  };


});



