/**
 * Created by Hellen Rojas Rojas on 03/08/2016.
 */


app.controller('VistaMisReservasAdminCtrl', function($scope,$firebaseArray,EvaluarReserva,$ionicPopup,$state) {

  var url = window.location.href;
  var ArregloRes = url.split("/");//cambiar cuando se cambie la url

  res = ArregloRes.pop();
  console.log(res);
  var refRes = new Firebase("https://sideappprueba.firebaseio.com/Reserva/");
  var refUsuario = new Firebase("https://sideappprueba.firebaseio.com/Persona/");

  refRes.child(res).once('value', function(snapshot) {
    console.log(snapshot.val());
    var arrayInfoBD = eval(snapshot.val());                 //almacena el objeto o JSON obtenido de firebase, pero para ser utilzado

    var chofRef = new Firebase("https://sideappprueba.firebaseio.com/Choferes");
    chofs = $firebaseArray(chofRef);




    //CARGAR CHOFERES EN LA LISTA
    var cargarChofer = function ()
    {
      $scope.chofers=[];
      if( arrayInfoBD.Chofer == 'No'){
        console.log(arrayInfoBD.Chofer );
        $scope.chofers.push("No");
        $scope.chofer = $scope.chofers[0];
      }

      else {
        if (arrayInfoBD.Chofer == 'Si') {
          $scope.chofers.push("-- Seleccione --");
          chofs.$loaded()
            .then(function (data) {

              angular.forEach(data, function (value) {
                console.log(value.Nombre);
                $scope.chofers.push(value.Nombre + ' ' + value.Apellidos);
              });

            });
          $scope.chofer = $scope.chofers[0];

        }
        else {


          chofs.$loaded()
            .then(function (data) {

              angular.forEach(data, function (value) {
                console.log(value.Nombre);
                $scope.chofers.push(value.Nombre + ' ' + value.Apellidos);
              });
              var i = 0;
              console.log($scope.chofers.length);
              for(i;i< $scope.chofers.length;i++){
                console.log("sd");
                if(arrayInfoBD.Chofer == $scope.chofers[i]){
                  $scope.chofer = $scope.chofers[i];
                }
              }
            });

        }
      }
    };
    cargarChofer();

    //CRAGAR OTROS ELEMENTOS
    $scope.placa = arrayInfoBD.Placa;
    $scope.lugar = arrayInfoBD.Lugar;
    $scope.hSalida = arrayInfoBD.HoraSalida;
    $scope.hLlegada = arrayInfoBD.HoraLlegada;
    $scope.fSalida = arrayInfoBD.FechaSalida;
    $scope.fLlegada = arrayInfoBD.FechaLlegada;
    $scope.numPas = arrayInfoBD.Pasajeros;
    $scope.estado = arrayInfoBD.Estado;

    //CARGAR MENSAJES DE BOTON
    if( $scope.estado == "Pendiente"){
      $scope.estadoBotonAceptar = "Aceptar";
      $scope.estadoBotonDenegar = "Denegar";

    }
    else if($scope.estado == "Aceptado")
    {
      $scope.estadoBotonAceptar = "Editar";
      $scope.estadoBotonDenegar = "Denegar";
    }
    else if($scope.estado == "Denegado")
    {
      $scope.estadoBotonAceptar = "Aceptar";
      $scope.estadoBotonDenegar = "Editar";
    }
    //CARGAR JUSTIFICACIÓN


    if(arrayInfoBD.Justificacion != ""){
      $scope.justificacionRechazo = arrayInfoBD.Justificacion;
    }

    refUsuario.child(arrayInfoBD.Usuario).once('value', function(snapshot) {

      var arrayInfoBDP= eval(snapshot.val());                 //almacena el objeto o JSON obtenido de firebase, pero para ser utilzado
      console.log(arrayInfoBDP);
      $scope.nombrePersonaReserva = arrayInfoBDP.Nombre+" "+arrayInfoBDP.Apellidos ;

    });


  });
  var usu = localStorage.getItem("user");
  console.log(usu);
  refUsuario.child(usu).once('value', function(snapshot) {

    var arrayInfoPersona= eval(snapshot.val());                 //almacena el objeto o JSON obtenido de firebase, pero para ser utilzado
    console.log(arrayInfoPersona);
    $scope.nombre = arrayInfoPersona.Nombre+" "+arrayInfoPersona.Apellidos ;

  });


  $scope.evaluar = function (estado, justificacion,chofer) {
    var data;
    if(estado == "Denegado"){
      if(justificacion == undefined){
        var alertPopup = $ionicPopup.alert({
          title: 'Error',
          template: 'Por favor indique una Justificación!'
        });
      }else{
        console.log(justificacion);
        data ={ID:res,Estado:estado,Justificacion:justificacion,Chofer:"Si"};
        EvaluarReserva.setEstado(data);

      }
    }
    else if(estado == "Aceptado"){
      if(chofer == '-- Seleccione --'){
        var alertPopup = $ionicPopup.alert({
          title: 'Error',
          template: 'Por favor indique una chofer!'
        });
      }
      else {
        data = {ID: res, Estado: estado, Justificacion: "",Chofer:chofer};
        EvaluarReserva.setEstado(data);


        $state.go('appAdmin.adminReservas');

      }
    }

  }

});
