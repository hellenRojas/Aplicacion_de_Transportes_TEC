/**
 * Created by Hellen Rojas Rojas on 03/08/2016.
 */

app.controller('AddVehic',function (ActualizarVehi,$scope,$state) {
  $scope.addVehic =function (placasD,imagenD,marcaD,modeloD,traccionD,pasajerosD,dependenciaD) {
    var data = {ID:placasD,marca:marcaD,modelo:modeloD,traccion:traccionD,capacidad:pasajerosD,dependencia:dependenciaD,placa:placasD};
    ActualizarVehi.AtVehiculo(data);
    $state.go("appAdmin.flotilla");
  }
  $scope.BorrarVehic = function () {
    $state.go("appAdmin.flotilla");
  }
})
