/**
 * Created by Hellen Rojas Rojas on 03/08/2016.
 */


app.controller('flotillaDet', function($scope,$firebaseArray,$cordovaCamera, $ionicPopover,ActualizarVehi,$state) {
  var usersRef = new Firebase('https://sideappprueba.firebaseio.com/Vehiculo');

  var url = window.location.href;
  var arreglo = url.split("/");//cambiar cuando se cambie la url
  var placa = arreglo.pop();  //obtiene la placa del URL y gestiona una búsqueda en firebase para obtener la información

  console.log(placa);

  $scope.placasD = "";
  $scope.traccionD =  "";
  $scope.pasajerosD = "";
  $scope.dependenciaD = "";
  $scope.estadosD =  "";
  $scope.imagenD = "";
  $scope.marcaD =  "";
  $scope.modeloD = "";

  usersRef.child(placa).once('value', function (snapshot) {
    console.log(snapshot.val());
    var exists = (snapshot.val() !== null);
    var arrayInfoBD = eval(snapshot.val());
    $scope.placasD = arrayInfoBD.Placa;
    $scope.traccionD =  arrayInfoBD.Traccion;
    $scope.pasajerosD = arrayInfoBD.Capacidad;
    $scope.dependenciaD = arrayInfoBD.Dependencia;
    $scope.estadosD =  arrayInfoBD.Estado;
    $scope.imagenD = arrayInfoBD.image;
    $scope.marcaD =  arrayInfoBD.Marca;
    $scope.modeloD = arrayInfoBD.Modelo;
  });

  var userReference = new Firebase("https://sideappprueba.firebaseio.com/Vehiculo/" + placa);


  $scope.upload = function(typef) {
    var typeofpic;
    if(typef === "camera"){
      typeofpic = Camera.PictureSourceType.CAMERA;
    }
    else if(typef ==="gallery"){
      typeofpic =  Camera.PictureSourceType.SAVEDPHOTOALBUM;
    }
    var options = {
      quality : 75,
      destinationType : Camera.DestinationType.DATA_URL,
      sourceType : typeofpic,
      allowEdit : true,
      encodingType: Camera.EncodingType.JPEG,
      popoverOptions: CameraPopoverOptions,
      targetWidth: 1500,
      targetHeight: 1000,
      saveToPhotoAlbum: false
    };
    $cordovaCamera.getPicture(options).then(function(imageData) {
      userReference.update({image: imageData}).then(function() {
        alert("Image has been uploaded");
      });
    }, function(error) {
      console.error(error);
    });


    //controlador para el menu contextual con las opciones de tomar video o usar imagenes de galería




  }
  $scope.updateData = function (marcaD,modeloD,traccionD,pasajerosD,dependenciaD) {
    var data = {ID:placa,marca:marcaD,modelo:modeloD,traccion:traccionD,capacidad:pasajerosD,dependencia:dependenciaD};
    ActualizarVehi.AtVehiculo(data);
  }

  $scope.BorrarVehic = function () {
    var data = {ID:placa};
    ActualizarVehi.BorraVehiculo(data);
    $state.go("appAdmin.flotilla");

  }

  $ionicPopover.fromTemplateUrl('templates/popover.html', {
    scope: $scope,
  }).then(function(popover) {
    $scope.popover = popover;
  });




});
