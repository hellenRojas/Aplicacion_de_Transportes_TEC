angular.module('starter.services', [])

.service('LoginService', function($q) {
    return {
        loginUser: function(name, pw) {
            var deferred = $q.defer();
            var promise = deferred.promise;

            var USERS_LOCATION = 'https://sideappprueba.firebaseio.com/Persona/';

            var usersRef = new Firebase(USERS_LOCATION);
            //console.log(usersRef.child($scope.data.username));
            if(name == null || pw == null){                           //valida que el usuario no sea nulo
                deferred.reject('Wrong credentials.');
                promise.success = function(fn) {
                    promise.then(fn);
                    return promise;
                }
                promise.error = function(fn) {
                    promise.then(null, fn);
                    return promise;
                }
                return promise;

            }
            else{
                usersRef.child(name).once('value', function(snapshot) {
                console.log(snapshot.val());
                var exists = (snapshot.val() !== null);
                var arrayInfoBD = eval(snapshot.val());                 //almacena el objeto o JSON obtenido de firebase, pero para ser utilzado
                if(exists){                                             //valida que el usuario exista en firebase
                    if(arrayInfoBD.Contrasena == pw){                   //valida la contraseña según el json obtenido de firebase
                        deferred.resolve(arrayInfoBD.Tipo);
                        /*$cookieStore.put("name",name);
                        console.log($cookies.get("name"));*/
                    }else{
                        deferred.reject('Wrong credentials.');

                    }
                }
                else{
                    deferred.reject('Wrong credentials.');
                    promise.success = function(fn) {
                        promise.then(fn);
                        return promise;
                    }
                    promise.error = function(fn) {
                        promise.then(null, fn);
                        return promise;
                    }
                    return promise;
                    }
                });
            }
            promise.success = function(fn) {
                promise.then(fn);
                return promise;
            }
            promise.error = function(fn) {
                promise.then(null, fn);
                return promise;
            }
            return promise;


            /*

            if (name == 'user' && pw == 'secret') {
                deferred.resolve('Welcome ' + name + '!');
            } else {
                deferred.reject('Wrong credentials.');
            }
            promise.success = function(fn) {
                promise.then(fn);
                return promise;
            }
            promise.error = function(fn) {
                promise.then(null, fn);
                return promise;
            }
            return promise;
        }*/
        }
    }
})





  .service('ReservasService', function($firebaseArray) {
    cargarReservas= function (listaMisReservas)
    {
      listaMisReservas = [];
      var ref = new Firebase("https://sideappprueba.firebaseio.com/Reserva");
      vehiculos = $firebaseArray(ref);
      vehiculos.$loaded()
        .then(function (data) {
          angular.forEach(data, function (value) {
             console.log(value.Placa);
            if ( value.Usuario == localStorage.getItem("user")) { // valida que no este reservado

            listaMisReservas.push({title: value.Lugar, fechaIni: value.FechaSalida});
            }

          });
          console.log(value.Placa);

        });
    };
    return{
      listaMisReservas: listaMisReservas,
      cargarRes :cargarReservas()
    }
  })

  /**
   * Servicio para actualizar el estado de una reserva, recibe un json con los datos a actualizar
   */

  .factory('EvaluarReserva',function () {
    var evaluandoReservas = function (data) {
      var userReference = new Firebase("https://sideappprueba.firebaseio.com/Reserva/" + data.ID);
      userReference.update({Estado: data.Estado,Justificacion:data.Justificacion,Chofer:data.Chofer});
    }
    return{
      setEstado: evaluandoReservas
    }
  })

    /**
     * Servicio para actualizar los datos de un vehiculo, recibe un json con los datos a actualizar
     */
  .factory('ActualizarVehi',function () {
    var ActVehiculo = function (data) {
      var userReference = new Firebase("https://sideappprueba.firebaseio.com/Vehiculo/" + data.ID);
      var send = {Marca:data.marca, Modelo:data.modelo,Traccion:data.traccion,Capacidad:data.capacidad,Dependencia:data.dependencia, Placa:data.placa};
      userReference.update(send);
    }
    var BorrarVehiculo = function (data) {
      var userReference = new Firebase("https://sideappprueba.firebaseio.com/Vehiculo/" + data.ID);
      userReference.remove();
    }
    // var AddVehiculo = function (data) {
    //   var userReference = new Firebase("https://sideappprueba.firebaseio.com/Vehiculo/" + data.ID);
    //   var send = {Marca:data.marca, Modelo:data.modelo,Traccion:data.traccion,Capacidad:data.capacidad,Dependencia:data.dependencia,Imagen:data.imagen};
    //   userReference.update();
    // }
    return{
      AtVehiculo: ActVehiculo,
      BorraVehiculo: BorrarVehiculo
    }
  })


.service('ConsultaChoferes', function ($firebaseArray) {
  var choferesReference = new Firebase("https://sideappprueba.firebaseio.com/Choferes");
  var listaChoferes = [];
  this.consultarChoferes = function (callback) {
    choferes = $firebaseArray(choferesReference);
    choferes.$loaded()
      .then(function (data) {
        angular.forEach(data, function (value) {

          listaChoferes.push({id: value.$id, nombre: value.Nombre, apellidos: value.Apellidos, estado: value.Estado});

        });
        callback(listaChoferes);
      });

  };


  this.consultarChoferesID = function (id) {
    var ref = choferesReference+"/"+id;
    var choferes = $firebaseArray(ref);
    return choferes;
  };
})



