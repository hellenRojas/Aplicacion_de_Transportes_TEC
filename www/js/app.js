// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers', 'starter.services', 'firebase'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider

 .state('login', {
      url: '/login',
      templateUrl: 'templates/login.html',
      controller: 'LoginCtrl'
  })


    .state('appAdmin', {
    url: '/appAdmin',
    abstract: true,
    templateUrl: 'templates/templatesAdmin/menuAdmin.html',
    controller: 'AppAdminCtrl'
  })

  .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/templatesUser/menuUsu.html',
    controller: 'AppCtrl'
  })

  .state('app.reservar', {
    url: '/reservar',
    views: {
      'menuContent': {
        templateUrl: 'templates/templatesUser/reservar.html',
         controller: 'ReservarTransCtrl'
      }
    }
  })

    .state('app.reservarFlot', {
      url: '/reservar/:id',
      views: {
        'menuContent': {
          templateUrl: 'templates/templatesUser/reservarDesFlot.html',
          controller: 'ReservarTransCtrl'
        }
      }
    })

    .state('appAdmin.adminReservas', {
    url: '/adminReservas',
    views: {
      'menuContent': {
        templateUrl: 'templates/templatesAdmin/reservasAdmin.html',
        controller: 'AdminResevasCtrl'
      }
    }
  })

  .state('app.inicio', {
    url: '/inicio',
    views: {
      'menuContent': {
        templateUrl: 'templates/templatesUser/inicioUser.html'
      }
    }
  })

  .state('appAdmin.inicioAdmin', {
    url: '/inicioAdmin',
    views: {
      'menuContent': {
        templateUrl: 'templates/templatesAdmin/inicioAdmin.html'
      }
    }
  })


  .state('app.flotilla', {
      url: '/flotillaU',
      views: {
        'menuContent': {
          templateUrl: 'templates/templatesUser/flotillaU.html',
          controller: 'FlotillasCtrl'
        }
      }
    })


    .state('appAdmin.flotilla', {
      url: '/flotillaA',
      views: {
        'menuContent': {
          templateUrl: 'templates/templatesAdmin/flotillaA.html',
          controller: 'FlotillasCtrl'
        }
      }
    })

    .state('app.flot', {
      url: '/flotillaU/:autoid',
      views: {
        'menuContent': {
          templateUrl: 'templates/templatesUser/flotDetUsuario.html',
          controller:'flotillaDet'
        }
      }
    })

    .state('appAdmin.flot', {
      url: '/flotillaA/:autoid',
      views: {
        'menuContent': {
          templateUrl: 'templates/templatesAdmin/flotDetAdmin.html',
          controller:'flotillaDet'
        }
      }
    })

    .state('appAdmin.controlReserva', {
      url: '/AdminReservas/:idRes',
      views: {
        'menuContent': {
          templateUrl: 'templates/templatesAdmin/vistaResevaAdmin.html',
          controller: 'VistaMisReservasAdminCtrl'
        }
      }
    })

    .state('appAdmin.controlChofer', {
      url: '/Choferes',
      views: {
        'menuContent': {
          templateUrl: 'templates/templatesAdmin/controlChoferes.html',
          controller: 'controlChoferes'
        }
      }
    })

    .state('app.playlists', {
      url: '/playlists',
      views: {
        'menuContent': {
          templateUrl: 'templates/templatesUser/reservasUsuario.html',
          controller: 'ReservasCtrl'
        }
      }
    })

    .state('app.AddVehic', {
      url: '/addVehic',
      views: {
        'menuContent': {
          templateUrl: 'templates/templatesAdmin/flotAddVehic.html',
          controller: 'AddVehic'
        }
      }
    })
  .state('app.single', {
    url: '/playlists/:playlistId',
    views: {
      'menuContent': {
        templateUrl: 'templates/templatesUser/vistaReservaUsu.html',
        controller: 'VistaMisReservasUsuCtrl'
      }
    }
  });
  // if none of the above states are matched, use this as the fallback
   $urlRouterProvider.otherwise('/login');
});
